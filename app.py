from flask import Flask
from flask import render_template
app = Flask(__name__)

@app.route('/')
def homepage():
    return render_template('base.html')

@app.route('/dinosaur/<slug_du_dinosaur>/')
def dinosaur(slug_du_dinosaur):
    return render_template('dinosaur.html', slug_du_dinosaur=slug_du_dinosaur)
    


        
