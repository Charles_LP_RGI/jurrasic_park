import os
import pytest
from app import app
from flask import url_for

@pytest.fixture
def client():
    app.config['TESTING'] = True
    client = app.test_client()
    with app.app_context():
        pass
    yield client

def test_base():
    response = client.get('/')

def test_dinosaur():
    response = client.get('/dinosaur/')
    
def test_brachiosaurus(img, slug_du_dinosaur, name, Name_Meaning, Diet, Height, Length, Weight):
    response = client.get('/dinosaur/brachiosaurus')
    assert "..\static\img\brachiosaurus.jpg" == img
    assert 'brachiosaurus' == slug_du_dinosaur
    assert 'Brachiosaurus' == name
    assert 'Arm lizard' == Name_Meaning
    assert 'Végétivore' == Diet
    assert '12' == Height
    assert '22' == Length
    assert '30000' == Weight